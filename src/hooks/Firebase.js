import firebase from 'firebase/app';
import 'firebase/firestone';

const firebaseConfig = firebase.initializeApp({
    apiKey: '',
    authDomain: '',
    databaseURL: '',
    projectId: '',
    storageBucket: '',
    apiId: '',
});

export { firebaseConfig as firebase };